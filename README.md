# Hugo SPDX

This hugo module contains the [SPDX license list] as data,
and a partial template that generates a license link from the copyright field in the site configuration.
The copyright field must be set to a valid SPDX license ID, so the full name and URL can be found in the data.

## Credits

The data in this module is taken from the [spdx license list].

## License

This module is [MIT](license.txt) licensed.

[spdx license list]: https://spdx.org/licenses/
